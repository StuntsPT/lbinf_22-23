# Assignment 01

## Project

### Deadline:

October 31st 2022


### Format:

PDF


### Delivery:

E-mail


### Details:

* Your project must have:
  * Brief introduction to the problem
  * Objectives
  * Main methods
  * Expected results
  * Group members and their tasks
* Specify which parts of the approach will be **automated**!


Don't forget a cover and **references**, of course.
